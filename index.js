var express = require('express'); //Use Express 
var app = express();  //Create an App using Express

app.get('/', function(req, res){  //All 'gets' execute the following


//Read our JSON File
var data = require("./data1.json");

//Create a reponse with this JSON
res.json(data)

});

app.listen(9000) //Create Server to Listen on Port 9000
