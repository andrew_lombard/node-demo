#Node, Express + Docker + Swift Demo

FROM node:argon
MAINTAINER Andrew Lombard <andrew.lombard@gmail.com>

#Get Latest from our Repo
RUN git clone https://bitbucket.org/andrew_lombard/node-demo

# Install app dependencies, npm packages
RUN cd /node-demo; npm install 

#Declare our Working Folder
WORKDIR /node-demo

