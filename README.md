# README #

A node + express docker demo for Swift on IOS.

### How do I get set up? ###

1. Create a Docker Image for this solution:

```
#!python
docker build -t alombard/nodeexpressdemo:v1 .
```